﻿using Goniometer_Controller.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Reflection;

namespace Goniometer_Tests
{
    /// <summary>
    ///This is a test class for MeasurementCollectionTest and is intended
    ///to contain all MeasurementCollectionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MeasurementCollectionTest
    {
        /// <summary>
        ///A test for FromCSV
        ///</summary>
        [TestMethod()]
        public void FromCSVLightTest()
        {
            MeasurementCollection actual = GetRawLight();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for FromCSV
        ///</summary>
        [TestMethod()]
        public void FromCSVStrayTest()
        {
            MeasurementCollection actual = GetRawStray();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetEstimateReading
        ///</summary>
        [TestMethod()]
        public void GetEstimateReadingTest()
        {
            MeasurementCollection target = GetRawStray();
            string key = MeasurementKeys.Illuminance;
            double theta = 0F; 
            double phi = 0F; 

            //based on values found in raw_stray
            double expectedTheta = 0;
            double expectedPhi = 0;
            double expectedValue = 0.008;

            MeasurementBase actual;
            actual = target.GetEstimateReading(key, theta, phi);

            Assert.AreEqual(key,            actual.Key);
            Assert.AreEqual(expectedTheta,  actual.Theta);
            Assert.AreEqual(expectedPhi,    actual.Phi);
            Assert.AreEqual(expectedValue,  actual.Value);
        }

        /// <summary>
        /// A test for InterpolateMeasurementsToGrid
        /// </summary>
        [TestMethod()]
        public void InterpolateMeasurementsToGridTest()
        {
            MeasurementCollection target = GetRawLight();
            double resolution = 0.5;

            MeasurementCollection actual = target.InterpolateMeasurementsToGrid(resolution);
            
            Assert.IsNotNull(actual);

            string testfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string testfile = "InterpolateMeasurementsToGrid_Test_Results.csv";

            using (StreamWriter sw = new StreamWriter(testfolder + "\\" + testfile, false))
            {
                sw.Write(actual.ToCSV());
            }
        }

        [TestMethod()]
        public void SubtractStrayTest()
        {
            MeasurementCollection target = GetRawLight();
            MeasurementCollection stray = GetRawStray();

            MeasurementCollection actual = target.SubstractStray(stray);
            
            Assert.IsNotNull(actual);

            string testfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string testfile = "SubtractStray_Test_Results.csv";

            using (StreamWriter sw = new StreamWriter(testfolder + "\\" + testfile, false))
            {
                sw.Write(actual.ToCSV());
            }
        }

        [TestMethod()]
        public void SubtractStray_Aligned_Test()
        {
            MeasurementCollection target = GetRawLightAligned();
            MeasurementCollection stray = GetRawStrayAligned();

            MeasurementCollection actual = target.SubstractStray(stray);

            Assert.IsNotNull(actual);

            string testfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string testfile = "SubtractStray_Test_Results.csv";

            using (StreamWriter sw = new StreamWriter(testfolder + "\\" + testfile, false))
            {
                sw.Write(actual.ToCSV());
            }
        }


        public static MeasurementCollection GetRawLight()
        {
            string testfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string testfile = "Example Data\\raw_light.csv";

            using (var sr = new StreamReader(testfolder + "\\" + testfile))
            {
                return MeasurementCollection.FromCSV(sr.ReadToEnd());
            }
        }

        public static MeasurementCollection GetRawStray()
        {
            string testfolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string testfile = "Example Data\\raw_stray.csv";

            using (var sr = new StreamReader(testfolder + "\\" + testfile))
            {
                return MeasurementCollection.FromCSV(sr.ReadToEnd());
            }
        }

        public static MeasurementCollection GetRawLightAligned()
        {
            MeasurementCollection raw = GetRawLight();
            double resolution = 0.5;

            return raw.InterpolateMeasurementsToGrid(resolution);
        }

        public static MeasurementCollection GetRawStrayAligned()
        {
            MeasurementCollection raw = GetRawStray();
            double resolution = 0.5;

            return raw.InterpolateMeasurementsToGrid(resolution);
        }
    }
}
